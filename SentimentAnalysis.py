from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
from textblob import TextBlob
import json

#consumer key, consumer secret, access token, access secret.
consumer_key ="Consumer_Key"
consumer_secret="Consumer_Secret"
access_token="Access_Token"
access_token_secret="Access_Token_Secret"


class listen_data(StreamListener):
    def on_data(self, data):
        all_data = json.loads(data)
        try:
            tweet = all_data['text']
            analysis = TextBlob(tweet)
            sentiment_analysis = analysis.sentiment
            print(tweet, sentiment_analysis)

            polarity_val = analysis.sentiment.polarity
            if polarity_val > 0:
                sent = 'pos'
            elif polarity_val == 0:
                sent = 'neut'
            elif polarity_val < 0 :
                sent = 'negt'

            output_file = open('Tweet_Analysis.txt','a')
            output_file.write('{} {} {}\n'.format(tweet, analysis.sentiment, sent))
            output_file.close()
            return True
        except:
            return True

    def on_error(self, status):
        print(status)

auth = OAuthHandler(consumer_key,consumer_secret)
auth.set_access_token(access_token,access_token_secret)

twitter_data_stream = Stream(auth,listen_data())
twitter_data_stream.filter(track=['Trump'])

